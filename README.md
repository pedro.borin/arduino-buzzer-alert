# Arduino buzzer alert

## Resumo

Projeto feito em arduino para simular um sistema de alerta onde, com ajuda de um sensor de luz, quando detectado um valor de luz acima do valor indicado, aciona o buzzer (1 segundo ligado e 1 segundo desligado) até que este valor abaixe novamente. Também é possível visualizar os valores detectados pelo sensor na tela LCD.

## Objetivo

Mostrar o funcionamento dos sensores e atuadores e testar a interação com os mesmos.

## Materiais

3 Resistores: 1kΩ, 17kΩ, 10kΩ<br>
1 protoboard<br>
1 buzzer<br>
1 LCD 16x2<br>
1 potenciômetro<br>
1 sensor de luz<br>
Arduino Uno R3<br>
Fios Jumper

## Projeto

Utilizamos os materiais listados para montar um circuito que toma base o Arduino Uno R3. No projeto fizemos a ligação do Arduino na Protoboard para dar energia e colocamos o sensor de luz com o resistor 10kΩ para melhorar a taxa de luminosidade enviada para o arduino e evitar danos ao sensor. Após inserirmos o sensor no código, fizemos a instalação do buzzer (dispositivo sonoro) com um resistor de 17kΩ para adequar o volume emitido. Por último, ao final da criação do código do sensor e do buzzer, acrescentamos o LCD e sua pinagem à protoboard e ao arduino para apresentar o valor aferido pelo sensor de luz, além de mostrar também o valor mínimo estipulado por nós como limite para o acionamento do buzzer. O código foi escrito de forma que, quando o valor detectado pelo sensor de luz seja maior que o valor estabelecido, o buzzer acionará como uma forma de alarme e irá alternar entre ligado e desligado com um intervalo de 1 segundo, até que esse valor desça novamente para abaixo do valor limite. O código também apresenta os valores obtidos pelo sensor de luz e o valor limite estabelecido.
