// C++ code
//
#include <LiquidCrystal.h>
#include <ctype.h>

LiquidCrystal lcd(12, 13, 7, 6, 5, 4); // Pinagem do LCD

int pinoSensorLuz = A1; // Sensor de luz
int valorLuz = 0;  // Valor dado pelo sensor de luz
int buzzer = 3;  // Buzzer

void setup()
{
  lcd.begin(16, 2); // Inicia o lcd de 16x2

  Serial.begin(9600);
  pinMode(buzzer,OUTPUT); // Buzzer

}

void mostrarDados(){ // função para mostrar os dados no LCD
  valorLuz = analogRead(pinoSensorLuz); // escaneia o valor da luz
  lcd.clear();
  lcd.setCursor(0, 0); // primeira linha
  lcd.print("Luz:");
  lcd.setCursor(5, 0); // primeira linha casa 5
  lcd.println(valorLuz); // printa o valor da luz
  lcd.setCursor(0, 1); // segunda linha
  lcd.print("Limite: 70"); // printa o limite estabelecido
}

//os valores do sensor de luz variam de 0 a 134

void loop(){

  mostrarDados(); // os dados são mostrados a cada meio segundo
  
  // o buzzer liga e desliga a cada um segundo
  // dentro da função if
  if(valorLuz>=71){ // Se valor da luz maior que 70
    mostrarDados(); 
    tone(buzzer,125); // buzzer settado com o valor de 125
    delay(500);
    mostrarDados();
    delay(500);
    
    noTone(buzzer); // buzzer desliga após um segundo
    
    delay(500);
    mostrarDados();
    delay(500);
    mostrarDados();

  }
  
  delay(500);

}
